import {defineSupportCode} from 'cucumber';
import HomePage from '../../page-objects/home-page';

defineSupportCode(function ({And, But, Given, Then, When}) {

    When(/^User accesses the zerowebappsecurity site$/, function () {

        browser.url('http://zero.webappsecurity.com/');

    });

    Then(/^The Home Page is displayed$/, function () {
            new HomePage();
    });
});