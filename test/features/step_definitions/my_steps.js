import {defineSupportCode} from 'cucumber';
import HomePage from '../../page-objects/home-page';
import OnlineBankingPage from '../../page-objects/online-bank-page';

defineSupportCode(function ({And, But, Given, Then, When}) {

    Given(/^I access the zero web app security page$/, function () {
        browser.url('http://zero.webappsecurity.com/');
    });

    Then(/^The Home page is displayed$/, function () {

        new HomePage();
    });

    Given(/^I click on the More Services button$/, function () {

        new HomePage().clickMoreServices();
    });

    Then(/^The Online Banking page is displayed$/, function () {

        new OnlineBankingPage();
    });

    When(/^User enters username: "([^"]*)"$/, function (username) {

        //Codes
    });

});
var myStepDefinitionsWrapper = function () {
    When(/^User accesses the "([^"]*)" site$/, function (arg1, callback) {
        callback.pending();
    });

};
module.exports = myStepDefinitionsWrapper;