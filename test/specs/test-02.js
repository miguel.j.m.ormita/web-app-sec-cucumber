//var assert = require('assert');
import expect from 'chai';
import NavigationTab from '../page-objects/navigation-tab';
import OnlineBankingPage from '../page-objects/online-banking-page'; 

describe('Web App Security site', function() {

    it('should display the Main Page', function() {
     
        //automation codes here
        browser.url(browser.config.baseUrl);

        //Click on the Online Banking tab
        let navigationTab = new NavigationTab();
        
        let onlineBankingPage = navigationTab.clickOnlineBankingTab();
                
        onlineBankingPage.clickAccountSummary();
    });    
  });


  
