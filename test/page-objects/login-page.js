
class LoginPage {

    constructor() {

        try{
            this.isPageDisplayed();
        }catch(err){
            throw new Error ('The '+this.pageName+' is not displayed');
        }
        
    }

    get pageName() { return 'Login Page';}  
    get loginHeaderText() { return $('//h3[contains(text(), "Log in to ZeroBank")]'); }

    isPageDisplayed() {
        this.loginHeaderText.waitForDisplayed(10000); 
    }
}

export default LoginPage;