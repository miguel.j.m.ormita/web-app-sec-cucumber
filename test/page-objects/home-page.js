import OnlineBankingPage from './online-bank-page';

class HomePage{

    constructor() {
        
        try{
              this.isPageDisplayed();
        }catch(err){
            throw new Error('The '+this.pageName+' is not displayed' + err);
        }
    
    }
    
    get pageName() { return 'Home Page'; }
    get activeHomeTab() { return $('//li[@class="active"][@id="homeMenu"]'); }
    get moreServiceButton() { return $('//a[text()="More Services"]'); }

    isPageDisplayed(){
        this.activeHomeTab.waitForVisible(10000);
    }

    clickMoreServices(){
        this.moreServiceButton.click();

        //return Online Banking Page
        return new OnlineBankingPage();
    }

}

export default HomePage;
